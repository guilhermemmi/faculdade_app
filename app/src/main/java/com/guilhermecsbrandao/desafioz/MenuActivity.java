package com.guilhermecsbrandao.desafioz;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.*;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.guilhermecsbrandao.desafioz.databinding.ActivityMenuBinding;
import com.guilhermecsbrandao.desafioz.databinding.NavHeaderMenuBinding;
import com.guilhermecsbrandao.desafioz.model.Student;

/**
 * Main screen. On that screen will be loaded some fragments based on
 * the User selection at the Side Menu.
 */
public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    // Binding da tela de menu
    ActivityMenuBinding menuBinding;

    // Binding do cabeçalho da Drawer
    NavHeaderMenuBinding navBinding;

    //AppBarMenuBinding appBarBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        menuBinding = DataBindingUtil.setContentView(MenuActivity.this, R.layout.activity_menu);

        navBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.nav_header_menu, menuBinding.navView, false);

        menuBinding.navView.setNavigationItemSelectedListener(this);

        menuBinding.navView.addHeaderView(navBinding.getRoot());

        Student student = (Student) getIntent().getSerializableExtra("student");

        menuBinding.setStudent(student);
        navBinding.setStudent(student);

        openMainFragment();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = menuBinding.drawerLayout;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            openMainFragment();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.

        FragmentManager fragmentManager;

        int id = item.getItemId();

        switch (id) {
            case R.id.drawer_school_records:
                SchoolRecordFragment fragment = SchoolRecordFragment.newInstance(menuBinding.getStudent());
                fragmentManager = getSupportFragmentManager();
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.content_menu, fragment, "schoolRecords")
                        .commit();
                break;

            case R.id.drawer_subjects:
                SubjectFragment fragmentSubjects = SubjectFragment.newInstance();
                fragmentManager = getSupportFragmentManager();
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.content_menu, fragmentSubjects, "fragmentSubjects")
                        .commit();
                break;

            case R.id.drawer_send:
                Intent it = new Intent(this, FeedbackActivity.class);
                it.putExtra("student", menuBinding.getStudent());
                startActivity(it);
                break;

        }

        menuBinding.drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * This method loads the SubjectListFragment (which represents the current period
     * the Student is attending to).
     */
    void openMainFragment() {
        SubjetctListFragment fragment = SubjetctListFragment.newInstance(menuBinding.getStudent());
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .replace(R.id.content_menu, fragment, "subjects")
                .commit();

    }

}
