package com.guilhermecsbrandao.desafioz.util;

import android.provider.BaseColumns;

/**
 * Created by guilherme on 18/05/17.
 */

/**
 * Utility class to hold tables, columns and DDL statements.
 */
public abstract class DbConstants implements BaseColumns {

    public static final String TB_SUPPORT_MESSAGE = "tb_supportmessage";
    public static final String SUBJECT = "subject";
    public static final String MESSAGE = "message";
    public static final String STUDENT_EMAIL = "studentMail";

    public static final StringBuilder CREATE_SUPPORT_MESSAGE_TABLE = new StringBuilder()
            .append("create table " + TB_SUPPORT_MESSAGE + " ( ")
            .append(_ID + " integer primary key autoincrement, ")
            .append(SUBJECT + " text, ")
            .append(MESSAGE + " text, ")
            .append(STUDENT_EMAIL + " text ); ");
}
