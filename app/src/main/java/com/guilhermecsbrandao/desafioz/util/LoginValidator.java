package com.guilhermecsbrandao.desafioz.util;

import android.text.TextUtils;

/**
 * Created by guilherme on 22/05/17.
 */

/**
 * Utility class to validate the login.
 */
public abstract class LoginValidator {

    static final String NUMBERS_ONLY = "^[0-9]*$";

    static boolean isValidCpf = false;
    static boolean isValidPass = false;


    /**
     * This method checks if the cpf is empty, has 11 digits and is numbers-only.
     * @param cpf String cpf to be validated.
     * @return Returns true if the CPF is valid.
     */
    public static boolean isValidCpf(String cpf) {

        if (!TextUtils.isEmpty(cpf)
                && cpf.length() == 11
                && cpf.matches(NUMBERS_ONLY)) {

            isValidCpf = true;
        }
        return isValidCpf;
    }

    /**
     * This method checks wheter if the password is empty and if it has spaces.
     * @param password String password to be validated.
     * @return Returns true if the Password is valid.
     */
    public static boolean isValidPass(String password) {

        if (!TextUtils.isEmpty(password)
                && !password.contains(" ")) {
            isValidPass = true;
        }

        return isValidPass;
    }
}
