package com.guilhermecsbrandao.desafioz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.guilhermecsbrandao.desafioz.database.FeedbackRepo;

import com.guilhermecsbrandao.desafioz.databinding.ActivityFeedbackBinding;
import com.guilhermecsbrandao.desafioz.model.Student;
import com.guilhermecsbrandao.desafioz.model.Feedback;

/**
 * Activity with the purpose to collect User's feedback and save on a database.
 */
public class FeedbackActivity extends AppCompatActivity {

    ActivityFeedbackBinding binding;
    Student student;
    View.OnClickListener sendMessageClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (insertOnSql(getSupportMessage()) != -1) {
                ProgressDialog dialog = ProgressDialog.show(FeedbackActivity.this, "",
                        getResources().getString(R.string.send_feedback), true);
                dialog.setIndeterminate(true);
                Toast.makeText(FeedbackActivity.this, "Message succefully saved", Toast.LENGTH_SHORT).show();
                Intent it = new Intent(FeedbackActivity.this, MenuActivity.class);
                it.putExtra("student", student);
                startActivity(it);
                finish();
            } else {
                Toast.makeText(FeedbackActivity.this, "Failed attempt to send message", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(FeedbackActivity.this, R.layout.activity_feedback);

        student = (Student) getIntent().getSerializableExtra("student");

        binding.buttonSendMessage.setOnClickListener(sendMessageClick);

    }

    /**
     * This method creates a Feedback object with the User's input.
     * @return Returns a filled Feedback object.
     */
    Feedback getSupportMessage() {
        Feedback message = new Feedback();

        message.setAssunto(binding.textViewSupportHeader.getText().toString());
        message.setMensagem(binding.textViewSupportBody.getText().toString());
        message.setEmailEstudante(student.getEmail());
        return message;
    }


    /**
     * This method inserts into the SQLite a Feedback sent by the User.
     * @param feedback Object Feedback filled with a Subject and Message.
     * @return long the row ID of the newly inserted row, or -1 if an error occurred.
     */
    long insertOnSql(Feedback feedback) {
        FeedbackRepo repo = new FeedbackRepo(this);
        return repo.insert(feedback);
    }

}
