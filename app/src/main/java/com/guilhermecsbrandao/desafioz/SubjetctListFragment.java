package com.guilhermecsbrandao.desafioz;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.guilhermecsbrandao.desafioz.adapter.SubjectListAdapter;
import com.guilhermecsbrandao.desafioz.databinding.FragmentSubjectListBinding;
import com.guilhermecsbrandao.desafioz.model.SchoolRecord;
import com.guilhermecsbrandao.desafioz.model.Student;

import java.util.List;


/**
 * Main fragment for the MenuActivity.
 */
public class SubjetctListFragment extends Fragment {

    FragmentSubjectListBinding subjetctListBinding;
    Student student;

    public SubjetctListFragment() {
        // Required empty public constructor
    }

    public static SubjetctListFragment newInstance(Student student) {

        Bundle args = new Bundle();
        args.putSerializable("student", student);

        SubjetctListFragment fragment = new SubjetctListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        subjetctListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_subject_list, container, false);

        student = (Student) getArguments().getSerializable("student");

        findSubjects();

        return subjetctListBinding.getRoot();

    }

    /**
     * This method loads the subjects which the user is attending at the current period.
     */
    void findSubjects() {

        List<SchoolRecord> records = student.getSchoolRecords();

        for (SchoolRecord record : records) {

            if (student.getCurrentPeriod().equals(record.getPeriod())) {
                subjetctListBinding.listSubjects.setAdapter(new SubjectListAdapter(getContext(),
                        android.R.layout.simple_list_item_1,
                        record.getSubjects()));
            }
        }

    }


}
