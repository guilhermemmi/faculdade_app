package com.guilhermecsbrandao.desafioz;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.guilhermecsbrandao.desafioz.databinding.ActivitySubjectDetailBinding;
import com.guilhermecsbrandao.desafioz.model.Subject;

/**
 * Activity to present details for a chosen Subject at the SubjectFragment.
 */
public class SubjectDetailActivity extends AppCompatActivity {

    ActivitySubjectDetailBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(SubjectDetailActivity.this, R.layout.activity_subject_detail);

        binding.setSubject((Subject) getIntent().getSerializableExtra("subject"));
    }
}
