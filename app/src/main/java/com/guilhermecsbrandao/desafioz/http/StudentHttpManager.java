package com.guilhermecsbrandao.desafioz.http;

import android.os.AsyncTask;

import com.google.gson.Gson;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.concurrent.ExecutionException;

import com.guilhermecsbrandao.desafioz.model.Student;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by guilherme on 15/05/17.
 */

/**
 * Class to manage the connection with the API for the Student model.
 */
public class StudentHttpManager {

    static final String STUDENTS_URL = "http://faculdadez.herokuapp.com/estudante?" +
            "type=login&molecule=estudante&cpf=";

    /**
     * This method sends a request to the API, parse the response from the
     * Json to Gson and retrun as array.
     *
     * @return Returns a Student registered on the database.
     * @throws IOException In case the request could not be executed due to cancellation,
     *                     a connectivity problem or timeout.
     */
    private Student getStudent(String cpf) throws IOException {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder().url(STUDENTS_URL + cpf).build();

        Response response = client.newCall(request).execute();
        if (response.networkResponse().code() == HttpURLConnection.HTTP_OK) {
            String json = response.body().string();

            Gson gson = new Gson();
            Student result =
                    gson.fromJson(json, Student.class);

            return result;
        }
        return null;
    }


    /**
     * This method executes de task in background.
     *
     * @param cpf String parameter to find an specific Student.
     * @return Return an object student filled with the database data.
     */
    public Student downloadStudent(String cpf) {
        DownloadStudentTask downloadTask = new DownloadStudentTask();
        downloadTask.execute(cpf);
        try {
            return downloadTask.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Class to create the task of download Students out of UI thread.
     */
    private class DownloadStudentTask extends AsyncTask<String, Void, Student> {

        @Override
        protected Student doInBackground(String... params) {
            try {
                return getStudent(params[0]);
            } catch (IOException e) {
                return null;
            }
        }
    }
}
