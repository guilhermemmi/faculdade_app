package com.guilhermecsbrandao.desafioz.http;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.guilhermecsbrandao.desafioz.model.Subject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.concurrent.ExecutionException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by guilherme on 16/05/17.
 */

/**
 * Class to manage the connection with the API for the Subject model.
 */
public class SubjectHttpManager {

    private static final String SUBJECTS_URL = "http://faculdadez.herokuapp.com/disciplina" +
            "?type=findAll&molecule=disciplina";

    /**
     * This method sends a request to the API, parse the response from the
     * Json to Gson and retrun as array.
     *
     * @return Returns an array of Subject registered on the database.
     * @throws IOException In case the request could not be executed due to cancellation,
     *                     a connectivity problem or timeout.
     */
    public Subject[] getSubjects() throws IOException {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder().url(SUBJECTS_URL).build();

        Response response = client.newCall(request).execute();
        if (response.networkResponse().code() == HttpURLConnection.HTTP_OK) {
            String json = response.body().string();

            Gson gson = new Gson();
            Subject[] result =
                    gson.fromJson(json, Subject[].class);
            if (result != null) {
                return result;
            }
        }
        return null;
    }

    /**
     * This method executes de task in background
     *
     * @return Returns an array of Subjects registered on the database.
     */
    public Subject[] downloadSubjects() {
        DownloadSubjectTask downloadTask = new DownloadSubjectTask();
        downloadTask.execute();
        try {
            return downloadTask.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Class to create the task of download Subjects out of UI thread.
     */
    private class DownloadSubjectTask extends AsyncTask<String, Void, Subject[]> {

        @Override
        protected Subject[] doInBackground(String... params) {
            try {
                return getSubjects();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }


}
