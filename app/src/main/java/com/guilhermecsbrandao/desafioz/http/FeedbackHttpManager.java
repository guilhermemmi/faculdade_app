package com.guilhermecsbrandao.desafioz.http;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.guilhermecsbrandao.desafioz.model.Feedback;
import com.guilhermecsbrandao.desafioz.model.Student;

import java.io.IOException;
import java.net.HttpURLConnection;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.guilhermecsbrandao.desafioz.http.StudentHttpManager.STUDENTS_URL;

/**
 * Created by guilherme on 19/05/17.
 */

public class FeedbackHttpManager {

    public static final String CREATE_FEEDBACK_URL = "http://faculdadez.herokuapp.com/feedback?" +
            "type=create&molecule=feedback&cpf=";

    public void createFeedback(Feedback feedback) throws IOException {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder().url(STUDENTS_URL).build();

        Response response = client.newCall(request).execute();
        if (response.networkResponse().code() == HttpURLConnection.HTTP_OK) {
            String json = response.body().string();

            Gson gson = new Gson();
            gson.toJson(feedback);


        }
    }
}
