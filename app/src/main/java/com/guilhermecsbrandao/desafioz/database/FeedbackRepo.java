package com.guilhermecsbrandao.desafioz.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.guilhermecsbrandao.desafioz.model.Feedback;
import com.guilhermecsbrandao.desafioz.util.DbConstants;

/**
 * Created by guilherme on 18/05/17.
 */

/**
 * Class to operate the DDL statment for the Feedback into the SQLite.
 */
public class FeedbackRepo {
    private SqlHelper helper;

    public FeedbackRepo(Context ctx) {
        helper = new SqlHelper(ctx);
    }

    /**
     * This method inserts into the SQLite a Feedback sent by the User.
     *
     * @param feedback Object Feedback filled with a Subject and Message.
     * @return long the row ID of the newly inserted row, or -1 if an error occurred.
     */
    public long insert(Feedback feedback) {
        SQLiteDatabase database = helper.getWritableDatabase();
        long id = database.insert(
                DbConstants.TB_SUPPORT_MESSAGE,
                null,
                getSupportMessageValues(feedback));

        if (id != -1) {
            feedback.setId(id);
        }

        database.close();
        return id;

    }

    /**
     * This method parses the object Feedback to ContentValues for being able to
     * be inserted on the database.
     *
     * @param feedback Object to be converted into ContentValues.
     * @return Returns a ContentValues filled with the Feedback data and based on SQLite columns.
     */
    ContentValues getSupportMessageValues(Feedback feedback) {
        ContentValues values = new ContentValues();
        values.put(DbConstants.SUBJECT, feedback.getAssunto());
        values.put(DbConstants.MESSAGE, feedback.getMensagem());
        values.put(DbConstants.STUDENT_EMAIL, feedback.getEmailEstudante());

        return values;
    }

}
