package com.guilhermecsbrandao.desafioz.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.guilhermecsbrandao.desafioz.util.DbConstants;


/**
 * Created by guilherme on 18/05/17.
 */

/**
 * Class to create and upgrade SQLite.
 */
public class SqlHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "desafioz.db";
    public static final int DB_VERSION = 1;
    private static SqlHelper instance;


    public SqlHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public static synchronized SqlHelper getInstance(Context ctx) {
        if (instance == null) {
            instance = new SqlHelper(ctx);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DbConstants.CREATE_SUPPORT_MESSAGE_TABLE.toString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            switch (oldVersion) {
                case 1:
            }
        }
    }
}
