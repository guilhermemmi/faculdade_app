package com.guilhermecsbrandao.desafioz;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.guilhermecsbrandao.desafioz.databinding.ActivityLoginBinding;
import com.guilhermecsbrandao.desafioz.http.StudentHttpManager;
import com.guilhermecsbrandao.desafioz.model.Student;
import com.guilhermecsbrandao.desafioz.util.LoginValidator;

public class LoginActivity extends AppCompatActivity {

    ActivityLoginBinding binding;
    View.OnClickListener loginClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            openMenuActivity(binding.editTextCpf.getText().toString(),
                    binding.editTextSenha.getText().toString());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(LoginActivity.this, R.layout.activity_login);


        binding.buttonLogin.setOnClickListener(loginClick);

    }

    /**
     * This method verify is User's inputs are valid, grab a student from our API,
     * checks if the password input is the same as the one registered on the database
     * and opens MenuActivity (which is the main screen).
     *
     * @param cpf  String User's input CPF.
     * @param pass String User's input Password.
     */
    private void openMenuActivity(String cpf, String pass) {

        Student student = null;
        if (validateLogin(cpf, pass)) {
            student = getStudent(cpf);

            // Dialog with Autheticating message
            ProgressDialog dialog = ProgressDialog.show(LoginActivity.this, "",
                    getResources().getString(R.string.authenticating), true);
            dialog.setIndeterminate(true);

        }

        // Checks if the user's input password is the same as registered.
        if (student != null && pass.equals(student.getPass())) {

            Intent it = new Intent(this, MenuActivity.class);
            it.putExtra("student", student);
            Toast.makeText(this, "Welcome back!", Toast.LENGTH_SHORT).show();
            startActivity(it);
        } else {
            Toast.makeText(this, "Failed to log in: Invalid cpf or password.", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * This method search for a Student registered on our API by its Cpf.
     *
     * @param cpf String representing Student's CPF.
     * @return Return a Student if found else return null.
     */
    private Student getStudent(String cpf) {
        try {
            return new StudentHttpManager().downloadStudent(cpf);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    /**
     * This method validates whether the inputs from the users are valid or not.
     *
     * @param cpf  String representing its CPF. Cannot be null, has to be numbers
     *             only and have exactly 11 characteres.
     * @param pass String representing user's password. Cannot be null.
     * @return Returns true if the user input is valid.
     */
    private boolean validateLogin(String cpf, String pass) {
        boolean validLogin;

        if (LoginValidator.isValidCpf(cpf)) {
            validLogin = true;
        } else {
            validLogin = false;
            binding.editTextCpf.setError("Invalid cpf");
        }

        if (LoginValidator.isValidPass(pass)) {
            validLogin = true;
        } else {
            validLogin = false;
            binding.editTextSenha.setError("Invalid Password");
        }

        return validLogin;
    }

}
