package com.guilhermecsbrandao.desafioz.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.guilhermecsbrandao.desafioz.R;
import com.guilhermecsbrandao.desafioz.databinding.SchoolrecordItemListBinding;
import com.guilhermecsbrandao.desafioz.databinding.SchoolrecordSubjectItemListBinding;
import com.guilhermecsbrandao.desafioz.model.SchoolRecord;
import com.guilhermecsbrandao.desafioz.model.Subject;

import java.util.List;

/**
 * Created by guilherme on 16/05/17.
 */

/**
 * Adpter for SchoolRecords List.
 */
public class SchoolRecordSimpleListAdapter extends ArrayAdapter<SchoolRecord> {

    Context context;
    SchoolrecordItemListBinding binding;

    public SchoolRecordSimpleListAdapter(@NonNull Context context, @LayoutRes int resource, List<SchoolRecord> schoolRecords) {
        super(context, resource, schoolRecords);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        binding = DataBindingUtil.
                inflate(inflater, R.layout.schoolrecord_item_list, null, false);

        binding.setSchoolRecord(getItem(position));

        binding.itemSchoolRecordListSubjects.setAdapter(new ListAdapter(context,
                android.R.layout.simple_list_item_2,
                getItem(position).getSubjects()));

        return binding.getRoot();
    }

    /**
     * Adapter for the List inside the SchoolRecordsList.
     */
    class ListAdapter extends ArrayAdapter<Subject> {

        Context context;
        SchoolrecordSubjectItemListBinding binding;

        public ListAdapter(@NonNull Context context, @LayoutRes int resource, List<Subject> subjects) {
            super(context, resource, subjects);
            this.context = context;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            LayoutInflater inflater = LayoutInflater.from(context);

            binding = DataBindingUtil.
                    inflate(inflater, R.layout.schoolrecord_subject_item_list, null, false);

            binding.setSubject(getItem(position));

            return binding.getRoot();
        }

    }
}
