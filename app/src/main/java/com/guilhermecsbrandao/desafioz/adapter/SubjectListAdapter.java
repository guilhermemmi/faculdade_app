package com.guilhermecsbrandao.desafioz.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.guilhermecsbrandao.desafioz.R;
import com.guilhermecsbrandao.desafioz.databinding.SubjectItemListBinding;
import com.guilhermecsbrandao.desafioz.model.Subject;

import java.util.List;

/**
 * Created by guilherme on 16/05/17.
 */

/**
 * Adapter for the SubjectList.
 */
public class SubjectListAdapter extends ArrayAdapter<Subject> {

    Context context;
    SubjectItemListBinding binding;

    public SubjectListAdapter(@NonNull Context context, @LayoutRes int resource, List<Subject> subjects) {
        super(context, resource, subjects);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        binding = DataBindingUtil.
                inflate(inflater, R.layout.subject_item_list, null, false);

        binding.setSubject(getItem(position));

        return binding.getRoot();
    }


}
