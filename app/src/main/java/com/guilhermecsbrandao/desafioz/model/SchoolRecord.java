package com.guilhermecsbrandao.desafioz.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import java.io.Serializable;
import java.util.List;

/**
 * Created by guilherme on 15/05/17.
 */

/**
 * Model class for SchoolRecord.
 */
public class SchoolRecord implements Serializable {

    @SerializedName("nota")
    @Expose
    private String grade;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("disciplinas")
    @Expose
    private List<Subject> subjects = null;

    @SerializedName("periodo")
    @Expose
    private String period;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }
}
