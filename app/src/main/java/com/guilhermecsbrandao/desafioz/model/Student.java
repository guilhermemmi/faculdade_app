package com.guilhermecsbrandao.desafioz.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by guilherme on 15/05/17.
 */

/**
 * Model class for Student.
 */
public class Student implements Serializable {

    @SerializedName("matricula")
    @Expose
    private String matriculation;

    @SerializedName("curso")
    @Expose
    private String course;

    @SerializedName("nome")
    @Expose
    private String name;

    @SerializedName("cpf")
    @Expose
    private String cpf;

    @SerializedName("email")
    @Expose
    private String email;


    @SerializedName("senha")
    @Expose
    private String pass;

    @SerializedName("periodo_atual")
    @Expose
    private String currentPeriod;

    @SerializedName("historico")
    @Expose
    private List<SchoolRecord> schoolRecords = null;

    public String getMatriculation() {
        return matriculation;
    }

    public void setMatriculation(String matriculation) {
        this.matriculation = matriculation;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getCurrentPeriod() {
        return currentPeriod;
    }

    public void setCurrentPeriod(String currentPeriod) {
        this.currentPeriod = currentPeriod;
    }

    public List<SchoolRecord> getSchoolRecords() {
        return schoolRecords;
    }

    public void setSchoolRecords(List<SchoolRecord> schoolRecords) {
        this.schoolRecords = schoolRecords;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("\nmtraticulation = " + this.matriculation)
                .append("\ncourse= " + this.course)
                .append("\nname= " + this.name)
                .append("\npass= " + this.pass)
                .append("\ncurrent period= " + this.currentPeriod)
                .toString();
    }


}
