package com.guilhermecsbrandao.desafioz.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by guilherme on 15/05/17.
 */

/**
 * Model class for Subject.
 */
public class Subject implements Serializable{

    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("nome")
    @Expose
    private String name;

    @SerializedName("horario")
    @Expose
    private String schedule;

    @SerializedName("dificuldade")
    @Expose
    private String difficulty;

    @SerializedName("nome_professor")
    @Expose
    private String teacherName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
