package com.guilhermecsbrandao.desafioz.model;

/**
 * Created by guilherme on 18/05/17.
 */

/**
 * Model class for Feedback.
 */
public class Feedback {
    private long id;
    private String assunto;
    private String mensagem;
    private String emailEstudante;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getEmailEstudante() {
        return emailEstudante;
    }

    public void setEmailEstudante(String emailEstudante) {
        this.emailEstudante = emailEstudante;
    }
}
