package com.guilhermecsbrandao.desafioz;


import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.guilhermecsbrandao.desafioz.adapter.SubjectListAdapter;
import com.guilhermecsbrandao.desafioz.databinding.FragmentSubjectBinding;
import com.guilhermecsbrandao.desafioz.http.SubjectHttpManager;
import com.guilhermecsbrandao.desafioz.model.Subject;

import java.util.Arrays;


/**
 * Fragment to load every Subject registered on database on the MenuActivity.
 */
public class SubjectFragment extends Fragment {


    FragmentSubjectBinding binding;
    AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent it = new Intent(getContext(), SubjectDetailActivity.class);
            it.putExtra("subject", (Subject)binding.listSubjectsWithDetail.getItemAtPosition(position));
            startActivity(it);
        }
    };

    public SubjectFragment() {
        // Required empty public constructor
    }

    public static SubjectFragment newInstance() {

        Bundle args = new Bundle();

        SubjectFragment fragment = new SubjectFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_subject, container, false);

        fillSubjects();

        binding.listSubjectsWithDetail.setOnItemClickListener(itemClickListener);

        return binding.getRoot();
    }

    /**
     * This method lists every Subject registered on database.
     * @return Returns an arroy ob Subjects.
     */
    private Subject[] getSubjects() {
        return new SubjectHttpManager().downloadSubjects();
    }

    /**
     * This method sets an Adapter for the ListView using the getSubjects method as data source.
     */
    void fillSubjects() {
        binding.listSubjectsWithDetail.setAdapter(new SubjectListAdapter(getContext(),
                android.R.layout.simple_list_item_1, Arrays.asList(getSubjects())));
    }

}
