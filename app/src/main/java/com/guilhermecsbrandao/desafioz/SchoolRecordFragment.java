package com.guilhermecsbrandao.desafioz;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.guilhermecsbrandao.desafioz.adapter.SchoolRecordSimpleListAdapter;
import com.guilhermecsbrandao.desafioz.databinding.FragmentSchoolRecordBinding;
import com.guilhermecsbrandao.desafioz.model.Student;

/**
 * Fragment to show an User's SchoolRecord on the MenuActivity.
 */
public class SchoolRecordFragment extends Fragment {

    FragmentSchoolRecordBinding recordBinding;

    Student student;

    public SchoolRecordFragment() {
        // Required empty public constructor
    }


    public static SchoolRecordFragment newInstance(Student student) {

        Bundle args = new Bundle();
        args.putSerializable("student", student);

        SchoolRecordFragment fragment = new SchoolRecordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        recordBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_school_record, container, false);

        student = (Student) getArguments().getSerializable("student");

        recordBinding.listRecords.setAdapter(new SchoolRecordSimpleListAdapter(getContext(),
                android.R.layout.simple_list_item_2,
                student.getSchoolRecords()));

        return recordBinding.getRoot();
    }
}
